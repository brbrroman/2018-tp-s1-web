# from django.conf.urls import urllib

# from questions.views import AboutView

# from questions import views
from django.urls import path #, include
from questions import views
from django.contrib.auth.decorators import login_required
from .models import *

urlpatterns = [
   path('ask/',       views.Ask.as_view(), name='questions_ask'),
   path('question/<int:pk>/',  views.QuestionDisplay.as_view(), name='question_detailed'),
   path('user/settings/',  views.settings, name='questions_settings'),
   path('user/<int:pk>/', views.user, name='question_user'),
   path('signin/',    views.signin, name='questions_signin'),
   path('login/', views.signin, name='questions_login'),
   path('logout/', views.signout, name='questions_logout'),
   path('accounts/login/', views.signin, name='questions_signin'),
   path('signup/',    views.signup, name='questions_signup'),
   path('hot/',       views.hot, name='questions_hot'),
   path('tag/<str:tag_title>/', views.tag, name='questions_tag'),
   path('index/', views.index, name='questions_index'),
   path('',          views.index, name='questions_index'),

   path('question/<int:pk>/like/',
      login_required(views.VotesView.as_view(model=Question, vote_type=LikeDislike.LIKE)),
       name='question_like'),
   path('question/<int:pk>/dislike/',
       login_required(views.VotesView.as_view(model=Question, vote_type=LikeDislike.DISLIKE)),
       name='question_dislike'),
   path('answer/<int:pk>/like/',
       login_required(views.VotesView.as_view(model=Answer, vote_type=LikeDislike.LIKE)),
       name='answer_like'),
   path('answer/<int:pk>/dislike/',
       login_required(views.VotesView.as_view(model=Answer, vote_type=LikeDislike.DISLIKE)),
       name='answer_dislike'),
]