from questions.models import *


def popular_tags(request):
    return {"popular_tags": Tag.objects.hottest()[:10]}


def best_members(request):
    return {"best_members": User.objects.hottest()[:10]}