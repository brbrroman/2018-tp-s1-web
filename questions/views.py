from django.shortcuts import render, redirect, get_object_or_404
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse, Http404
from django.views import View
import json

from .forms import *
from .models import LikeDislike
from django.views.generic import TemplateView


def paginatedRender(request, template, context):
    paginator = Paginator(context['page'], 20)
    page = request.GET.get('page')
    context['page'] = paginator.get_page(page)
    return render(request, template, context)


def index(request):
    questions = Question.objects.newest()
    template = 'questions_list.html'
    context = {'header': 'Новые вопросы', 'page': questions }
    return paginatedRender(request, template, context)


def hot(request):
    questions = Question.objects.hottest()
    template = 'questions_list.html'
    context = {'header': 'Популярные вопросы', 'page': questions }
    return paginatedRender(request, template, context)


def tag(request, tag_title):
    questions = Tag.objects.by_tag(tag_title)
    template = 'questions_list.html'
    context = {'header': 'Поиск по тегу: ' + tag_title, 'page': questions }
    return paginatedRender(request, template, context)


def user(request, pk):
    user = get_object_or_404(User, id=pk)
    questions = Question.objects.by_user(user_id=user.id)
    template = 'user.html'
    context = {'header': '', 'page': questions, 'user': user }
    return paginatedRender(request, template, context)


class QuestionDisplay(View):
    def get(self, request, pk):
        question = get_object_or_404(Question, id=pk)
        alerts = []
        form = AnswerForm()
        answers = question.answer_set.oldest()
        template = 'question_detailed.html'
        context = {'QA': question, 'header': question.title, 'alerts': alerts, 'form': form, 'button_name': "Ответить", 'page': answers}
        return paginatedRender(request, template, context)

    def post(self, request, pk):
        question = get_object_or_404(Question, id=pk)
        alerts = []
        form = AnswerForm(request.POST)
        if form.is_valid():
            answer = Answer.objects.create(author=request.user, question_id=question.id, text=request.POST['text'])
            answer.save()
            return redirect('question_detailed', pk=question.id)
        else:
            alerts.append("Что-то пошло не так")
        answers = question.answer_set.oldest()
        template = 'question_detailed.html'
        context = {'QA': question, 'header': question.title, 'alerts': alerts, 'form': form, 'button_name': "Ответить", 'page': answers}
        return paginatedRender(request, template, context)


class Ask(View):
    def get(self, request):
        alerts = []
        form = AskForm()
        template = 'ask.html'
        context = {'form': form, 'alerts': alerts, 'header': 'Добавить вопрос', 'button_name': "Задать вопрос сообществу"}
        return render(request, template, context)

    def post(self, request):
        alerts = []
        form = AskForm(request.POST)
        if form.is_valid():
            question = form.save(commit=False)
            question.author =request.user
            for tagTitle in request.POST['tags'].split(): # form.cleaned_data.get("tags")
                tag = Tag.objects.get_or_create(title=tagTitle)
                question.tags.add(tag)
            question.save()
            return redirect('question_detailed', pk=question.id)
        else:
            alerts.append("Что-то пошло не так")
        template = 'ask.html'
        context = {'form': form, 'alerts': alerts, 'header': 'Добавить вопрос', 'button_name': "Задать вопрос сообществу"}
        return render(request, template, context)


@login_required
def settings(request):
    user=request.user
    form = UserSettingsForm(instance=user)
    if request.method == 'POST':
        form = UserSettingsForm(instance=user,
                                data=request.POST,
                                files=request.FILES
                                )
        if form.is_valid():
            form.save()
            return redirect('questions_settings')

    template = 'form.html'
    context = {'header': 'Настройки пользователя: ' + user.username, 'form': form, 'button_name': "Сохранить",}
    return render(request, template, context)


@login_required
def signout(request):
    if not request.user.is_authenticated:
        raise Http404
    logout(request)
    return redirect('/')


def signin(request):
    alerts = []
    form = SignInForm
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is None:
            alerts.append('Неправильные данные для входа')
        else:
            login(request, user)
            return redirect(request.GET.get('next') if request.GET.get('next') else '/')

    logout(request)
    template = 'form.html'
    context = {'form': form, 'header': 'Вход', 'alerts': alerts, 'button_name': "Войти",}
    return render(request, template, context)


def signup(request):
    form = SignUpForm
    alerts= []
    if request.method == 'POST':
        form = form(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('/')
        else:
            alerts.append('Данный никнейм или email уже заняты')
    else:
        logout(request)

    template = 'form.html'
    context = {'form': form, 'header': 'Регистрация', 'alerts': alerts, 'button_name': "Зарегистрироваться",}
    return render(request, template, context)


class VotesView(View):
    model = None  # Data Model - Question or Answer
    vote_type = None  # Vote type Like/Dislike

    def post(self, request, pk):
        obj = self.model.objects.get(pk=pk)
        # GenericForeignKey does not support get_or_create
        try:
            likedislike = LikeDislike.objects.get(
                content_type=ContentType.objects.get_for_model(obj),
                object_id=obj.id,
                user=request.user
            )
            if likedislike.vote is not self.vote_type:
                likedislike.vote = self.vote_type
                likedislike.save(update_fields=['vote'])
                result = True
            else:
                likedislike.delete()
                result = False
        except LikeDislike.DoesNotExist:
            obj.votes.create(user=request.user, vote=self.vote_type)
            result = True

        return HttpResponse(
            json.dumps({
                "result": result,
                "like_count": obj.votes.likes().count(),
                "dislike_count": obj.votes.dislikes().count(),
                "sum_rating": obj.votes.sum_rating()
            }),
            content_type="application/json"
        )

