from django import forms

from .models import *

class AskForm(forms.ModelForm):
    title = forms.CharField(
        label = 'Заголовок вопроса',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'maxlength': 120,
                'minlength': 10,
                'placeholder': 'How to land an Orbital Rocket Booster?'
            }
        )
    )

    text = forms.CharField(
        label='Текст вопроса',
        widget=forms.Textarea(
            attrs={
                'class': 'form-control',
                'minlength': 10,
                'placeholder': 'Текст вопроса'
            }
        )
    )

    tags = forms.CharField(
        required=False,
        label='Теги',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'rockets, boosters'
            }
        )
    )

    class Meta:
        model = Question
        fields = ('title', 'text', 'tags')

class SignInForm(forms.ModelForm):
    username = forms.CharField(
        label = 'Никнейм',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'maxlength': 50,
                'minlength': 2,
                'placeholder': 'Login'
            }
        )
    )

    password = forms.CharField(
        label='Пароль',
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'minlength': 8,
                'placeholder': 'Password'
            }
        )
    )

    class Meta:
        model = User
        fields = ('username', 'password')

class SignUpForm(forms.ModelForm):
    username = forms.CharField(
        label = 'Никнейм',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'minlength': 2,
                'placeholder': 'admin'
            }
        )
    )

    email = forms.EmailField(
        label = 'Email',
        widget=forms.EmailInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'admin@admin.ru'
            }
        )
    )

    password = forms.CharField(
        label='Пароль',
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'minlength': 8,
                'placeholder': 'Password',
                'data-toggle': "password"
            }
        ),
    )

    class Meta:
        model = User
        fields = ('username', 'email', 'password')

class AnswerForm(forms.ModelForm):
    text = forms.CharField(
        label='Ваш ответ',
        widget=forms.Textarea(
            attrs={
                'class': 'form-control',
                'rows': 4,
                'placeholder': 'Запишите ваш ответ сюда'
            }
        )
    )

    class Meta:
        model = Answer
        fields = ('text', )


class UserSettingsForm(forms.ModelForm):
    username = forms.CharField(
        required=False,
        label = 'Никнейм',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'maxlength': 50,
                'minlength': 2,
                'placeholder': 'username'
            }
        )
    )

    email = forms.EmailField(
        label = 'Email',
        required=False,
        widget=forms.EmailInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'email'
            }
        )
    )
    # image = forms.ImageField(
    #     label='Аватарка',
    #     required=False,
    #     widget=forms.FileInput(
    #         attrs={
    #             'class': 'form-control',
    #             'placeholder': 'Выбрать файл'
    #         }
    #     )
    # )

    class Meta:
        model = User
        fields = ['username', 'email', 'avatar']
