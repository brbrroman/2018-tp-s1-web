from django.contrib.auth.models import AbstractUser
from django.shortcuts import reverse
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.fields import GenericRelation
from .managers import *
from django_bulk_update.manager import BulkUpdateManager


class User(AbstractUser):
    avatar = models.ImageField(default="img/emptyUser.png", upload_to='uploads/%Y/%m/%d/')

    objects = UserManager()

    def get_absolute_url(self):
        return reverse('question_user', args=[self.id])

    def __str__(self):
        return '@' + self.username


class Tag(models.Model):
    title = models.CharField(max_length=30, unique=True, verbose_name=u"Заголовок ярлыка")

    def __str__(self):
        return '#'+self.title

    def get_absolute_url(self):
        return reverse('questions_tag', args=[self.title])

    objects = TagManager()


class LikeDislike(models.Model):
    LIKE = 1
    DISLIKE = -1

    VOTES = (
        (LIKE, 'Like'),
        (DISLIKE, 'Dislike')
    )

    vote = models.SmallIntegerField(default=VOTES[0], choices=VOTES, verbose_name=u'Голос')
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name=u'Пользователь')

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()

    objects = LikeDislikeManager()

    def __str__(self):
        return "Like from " + self.user.username

    class Meta:
        #unique_together = ('user', 'content_object')
        indexes = [
            models.Index(fields=['content_type',]),
        ]


class Question(models.Model):
    author = models.ForeignKey(User, on_delete=models.PROTECT)

    title = models.CharField(max_length=128, verbose_name=u"Заголовок вопроса")
    text = models.TextField(verbose_name=u"Полное описание вопроса")
    is_active = models.BooleanField(default=True, verbose_name=u'Доступность вопроса')
    tags = models.ManyToManyField(Tag, blank=True, verbose_name=u'Теги')
    votes = GenericRelation(LikeDislike, related_query_name='questions')
    rate = models.IntegerField(default=0, verbose_name=u'Рейтинг')
    answer_count = models.IntegerField(default=0, verbose_name=u'Количество ответов')

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = QuestionManager()
    bulk_objects = BulkUpdateManager()

    def __str__(self):
        return self.text

    def get_absolute_url(self):
        return reverse('question_detailed', args=[self.id])

    class Meta:
        ordering = ['-created_at', '-updated_at']


class Answer(models.Model):
    author = models.ForeignKey(User, on_delete=models.PROTECT)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    text = models.TextField(verbose_name=u"Полное описание ответа")
    votes = GenericRelation(LikeDislike, related_query_name='answers')
    rate = models.IntegerField(default=0, verbose_name=u'Рейтинг')

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = AnswerManager()

    def __str__(self):
        return self.text

    class Meta:
        indexes = [
            models.Index(fields=['question',]),
        ]
        ordering = ['-created_at', '-updated_at']


@receiver(post_save, sender=Answer)
def inc_question_answer_count(sender, instance, created, **kwargs):
    if created:
        instance.question.answer_count += 1
        instance.question.save()


@receiver(post_delete, sender=Answer)
def dec_question_answer_count(sender, instance, created, **kwargs):
    instance.question.answer_count -= 1
    instance.question.save()